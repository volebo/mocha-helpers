# CHANGELOG

```
################################################################################
#                                                                              #
# db    db  .8888.  dP     888888b 8888ba   .8888.     d8b   db 888888b d8888P #
# 88    88 d8'  `8b 88     88      88  `8b d8'  `8b    88V8  88 88        88   #
# Y8    8P 88    88 88    a88aaa   88aa8P' 88    88    88 V8 88 88aaa     88   #
# `8b  d8' 88    88 88     88      88  `8b 88    88    88  V888 88        88   #
#  `8bd8'  Y8.  .8P 88     88      88  .88 Y8.  .8P dP 88   V88 88        88   #
#    YP     `888P'  88888P 888888P 888888'  `888P'  88 VP    8P 888888P   dP   #
#                                                                              #
################################################################################
```

--------------------------------------------------------------------------------
>
> All notable changes to this project will be documented in this file.
>
> The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
> and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
>
> **This file MUST be filled only by maintainers, using messages from pull**
> **requests.**
>
> new release template
>
> ## [Unreleased] - yyyy-mm-dd
>
> ### Added
> ### Changed
> ### Fixed
> ### Removed
>
--------------------------------------------------------------------------------

## [0.3.1] - 2024-10-22
## [0.3.0] - 2024-10-21

### Changed

- moved to ESM
- added Mocha Hooks `hooksRenameRootSuites` for setting up names for
  test suites automatically

--------------------------------------------------------------------------------

## [0.2.6] - 2020-07-13
## [0.2.5] - 2020-07-13
## [0.2.4] - 2020-07-13
## [0.2.3] - 2020-07-13
## [0.2.2] - 2020-07-13
## [0.2.1] - 2020-07-13

### Changed

- auto-build and deploy from GitLab Pipeline

## [0.2.0] - 2020-07-12

Bump version, remove obsolete stuff

### Added

- `tags`

### Changed

- update dependencies

### Removed

- `describeTag`
- `itTag`
- `describe`

--------------------------------------------------------------------------------

## [0.1.0] - 2017-07-23

INIT.

### Added

- `filename2suitename(__filename)`
- `describeTag`
- `itTag`
- `describe`

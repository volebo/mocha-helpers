# mocha-helpers

Useful tools for `mocha` in one package

--------------------------------------------------------------------------------

#### filename2suitename

generates the name for the root test-suite in the file.

```javascript

'use strict'

describe(filename2suitename(__filename), () => {
	// your tests
})

```

--------------------------------------------------------------------------------

#### tags

**provided by [mocha-tags](https://www.npmjs.com/package/mocha-tags)**. provides
an ability to filter TESTS by tags

A. use in your test files:

```javascript
tags(
	'slow',           // describe tags for the root test
	'network',
).describe(filename2suitename(__filename), () => {
	// your tests
})
```

B. call `mocha` with filtered tests

```bash
mocha --tags "not:slow is:network"
```

================================================================================

## Contributing

You could take part in the development process, just follow this [guideline](CONTRIBUTING.md).

## License

Please, read the [`LICENSE`](LICENSE) file in the root of the repository (or
downloaded package).

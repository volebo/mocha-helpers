/*
################################################################################
#                                                                              #
# db    db  .8888.  dP     888888b 8888ba   .8888.     d8b   db 888888b d8888P #
# 88    88 d8'  `8b 88     88      88  `8b d8'  `8b    88V8  88 88        88   #
# Y8    8P 88    88 88    a88aaa   88aa8P' 88    88    88 V8 88 88aaa     88   #
# `8b  d8' 88    88 88     88      88  `8b 88    88    88  V888 88        88   #
#  `8bd8'  Y8.  .8P 88     88      88  .88 Y8.  .8P dP 88   V88 88        88   #
#    YP     `888P'  88888P 888888P 888888'  `888P'  88 VP    8P 888888P   dP   #
#                                                                              #
################################################################################
#
# Mocha helpers for Volebo
#
# Copyright (C) 2016-2020 Volebo <dev@volebo.net>
# Copyright (C) 2016-2020 Maksim Koryukov <maxkoryukov@volebo.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the MIT License, attached to this software package.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
# You should have received a copy of the MIT License along with this
# program. If not, see <https://opensource.org/licenses/MIT>.
#
# http://spdx.org/licenses/MIT
#
################################################################################
*/


import $path               from 'node:path'
import { fileURLToPath }   from 'node:url'

export { default as tags } from 'mocha-tags'


export function filename2suitename (filename) {
	return $path.relative(process.cwd(), filename)
		.toLowerCase()
}


export function filename2suitenameEsm (importMeta) {

	const _pth = fileURLToPath(importMeta.url)
	return $path.relative(process.cwd(), _pth)
}


export const hooksRenameRootSuites = {
	beforeAll () {

		for (const suite of this?.test?.parent?.suites) {
			const _title = suite?.title
			const _renameSuite = ('undefined' === typeof _title) || ('__filename' === _title)
			if (true === _renameSuite) {
				const testFilename = $path.relative(process.cwd(), suite?.file)
				suite.title = testFilename
			}
		}
	},
}
